package org.josuealvarez.ui;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
public class VentanaPlantilla extends JFrame {
    
    private String titulo;
    private int alto;
    private int ancho;
    private JTable tblDatos;
    private JScrollPane scrTablaDatos; 
    private JLabel lblTitulo;
    private JLabel lblBarras;
    
    public VentanaPlantilla(String titulo, int ancho, int alto){
       this.titulo=titulo;
       this.ancho=ancho;
       this.alto=alto;
       this.setLayout(null);
       getContentPane().add(getLblTitulo());
       getContentPane().add(getScrTablaDatos());
       this.setSize(alto, ancho); 
       this.setTitle(titulo);
       this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       this.setVisible(true);       
    }
    
    public VentanaPlantilla(){
        //constructor nulo
    }
    
    public JTable getTblDatos() {
        if(tblDatos == null){
            tblDatos=new JTable ();
        }
        return tblDatos;
    }

    public void setTblDatos(JTable tblDatos) {
        this.tblDatos = tblDatos;
    }

    public JScrollPane getScrTablaDatos() {
        if(scrTablaDatos == null){
            scrTablaDatos = new JScrollPane();
            scrTablaDatos.setViewportView(getTblDatos());
            scrTablaDatos.setBounds(10, 40, 415, 350);
        }
        return scrTablaDatos;
    }

    public void setScrTablaDatos(JScrollPane scrTablaDatos) {
        this.scrTablaDatos = scrTablaDatos;
    }

    

    public JLabel getLblTitulo() {
        if(lblTitulo == null){
            lblTitulo=new JLabel(getTitulo());
            lblTitulo.setBounds(10, 5, 200, 40);
        }
        return lblTitulo;
    }

    public void setLblTitulo(JLabel lblTitulo) {
        this.lblTitulo = lblTitulo;
    }

    public JLabel getLblBarras() {
        return lblBarras;
    }

    public void setLblBarras(JLabel lblBarras) {
        this.lblBarras = lblBarras;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getAlto() {
        return alto;
    }

    public void setAlto(int alto) {
        this.alto = alto;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }
    
    

}
