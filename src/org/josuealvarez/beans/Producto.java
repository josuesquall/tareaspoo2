/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.josuealvarez.beans;

/**
 *
 * @author Fam-Alvarez
 */
public class Producto {
    //atributos
    private int codigo;
    private int codigoCategoria;
    private String descripcion;
    private double precioUnitario;
    private double precioDocena;
    private double precioMayor;
    private int existencia;
    private String tipoEmpaque;
    
    //contructor nulo
    public Producto(){
        
    }

    public Producto(int codigo, int codigoCategoria, String descripcion, double precioUnitario, double precioDocena, double precioMayor, int existencia, String tipoEmpaque) {
        this.codigo = codigo;
        this.codigoCategoria = codigoCategoria;
        this.descripcion = descripcion;
        this.precioUnitario = precioUnitario;
        this.precioDocena = precioDocena;
        this.precioMayor = precioMayor;
        this.existencia = existencia;
        this.tipoEmpaque = tipoEmpaque;
    }
    
    public Producto(int codigo, String descripcion, double precioUnitario, double precioDocena, double precioMayor, int existencia, String tipoEmpaque) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precioUnitario = precioUnitario;
        this.precioDocena = precioDocena;
        this.precioMayor = precioMayor;
        this.existencia = existencia;
        this.tipoEmpaque = tipoEmpaque;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigoCategoria() {
        return codigoCategoria;
    }

    public void setCodigoCategoria(int codigoCategoria) {
        this.codigoCategoria = codigoCategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getPrecioDocena() {
        return precioDocena;
    }

    public void setPrecioDocena(double precioDocena) {
        this.precioDocena = precioDocena;
    }

    public double getPrecioMayor() {
        return precioMayor;
    }

    public void setPrecioMayor(double precioMayor) {
        this.precioMayor = precioMayor;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public String getTipoEmpaque() {
        return tipoEmpaque;
    }

    public void setTipoEmpaque(String tipoEmpaque) {
        this.tipoEmpaque = tipoEmpaque;
    }
    
    
    
}
