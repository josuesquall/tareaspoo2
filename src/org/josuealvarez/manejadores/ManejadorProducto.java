/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.josuealvarez.manejadores;
import java.util.ArrayList;
import org.josuealvarez.beans.Producto;
/**
 *
 * @author Fam-Alvarez
 */
public class ManejadorProducto {
    private ArrayList<Producto> listaProductos =  new ArrayList<Producto>();

    public ManejadorProducto() {
    }
    
    public void agregar(Producto elemento){
        listaProductos.add(elemento);
    }
    
    public void eliminar(Producto elemento){
        listaProductos.remove(elemento);
    }
    
    public Producto buscar(int codigo){
        Producto resultado = null;
        for(Producto elemento : listaProductos){
            if(elemento.getCodigo()==codigo){
                resultado = elemento;
                break;
            }
        }
        return resultado;
    }
    
    public Producto getProducto(int posicion){
        return listaProductos.get(posicion);
    }

    public ArrayList<Producto> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(ArrayList<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }
}
