/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.josuealvarez.manejadores;
import java.util.ArrayList;
import org.josuealvarez.beans.Cliente;
/**
 *
 * @author Fam-Alvarez
 */
public class ManejadorCliente {
    private ArrayList<Cliente> listaClientes =  new ArrayList<Cliente>();

    public ManejadorCliente() {
    }
    
    public void agregar(Cliente elemento){
        listaClientes.add(elemento);
    }
    
    public void eliminar(Cliente elemento){
        listaClientes.remove(elemento);
    }
    
    public Cliente buscar(String nit){
        Cliente resultado = null;
        for(Cliente elemento : listaClientes){
            if(elemento.getNit().equalsIgnoreCase(nit)){
                resultado = elemento;
                break;
            }
        }
        return resultado;
    }
    
    public Cliente getCliente(int posicion){
        return listaClientes.get(posicion);
    }

    public ArrayList<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(ArrayList<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }
 
}
