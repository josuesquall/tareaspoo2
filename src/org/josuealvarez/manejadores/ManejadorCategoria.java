/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.josuealvarez.manejadores;
import java.util.ArrayList;
import org.josuealvarez.beans.Categoria;

/**
 *
 * @author Fam-Alvarez
 */
public class ManejadorCategoria {
    private ArrayList<Categoria> listaCategorias =  new ArrayList<Categoria>();

    public ManejadorCategoria() {
    }
    
    public void agregar(Categoria elemento){
        listaCategorias.add(elemento);
    }
    
    public void eliminar(Categoria elemento){
        listaCategorias.remove(elemento);
    }
    
    public Categoria buscar(int codigo){
        Categoria resultado = null;
        for(Categoria elemento : listaCategorias){
            if(elemento.getCodigo()==codigo){
                resultado = elemento;
                break;
            }
        }
        return resultado;
    }
    
    public Categoria getCategoria(int posicion){
        return listaCategorias.get(posicion);
    }

    public ArrayList<Categoria> getListaCategorias() {
        return listaCategorias;
    }

    public void setListaCategorias(ArrayList<Categoria> listaCategorias) {
        this.listaCategorias = listaCategorias;
    }
}
