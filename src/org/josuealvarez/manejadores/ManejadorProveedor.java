/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.josuealvarez.manejadores;
import java.util.ArrayList;
import org.josuealvarez.beans.Proveedor;
/**
 *
 * @author Fam-Alvarez
 */
public class ManejadorProveedor {
    private ArrayList<Proveedor> listaProveedores =  new ArrayList<Proveedor>();

    public ManejadorProveedor() {
    }
    
    public void agregar(Proveedor elemento){
        listaProveedores.add(elemento);
    }
    
    public void eliminar(Proveedor elemento){
        listaProveedores.remove(elemento);
    }
    
    public Proveedor buscar(int codigo){
        Proveedor resultado = null;
        for(Proveedor elemento : listaProveedores){
            if(elemento.getCodigoProveedor()==codigo){
                resultado = elemento;
                break;
            }
        }
        return resultado;
    }
    
    public Proveedor getProveedor(int posicion){
        return listaProveedores.get(posicion);
    }

    public ArrayList<Proveedor> getListaProveedors() {
        return listaProveedores;
    }

    public void setListaProveedors(ArrayList<Proveedor> listaProveedors) {
        this.listaProveedores = listaProveedors;
    }
}
